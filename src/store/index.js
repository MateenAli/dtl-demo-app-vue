import { createStore } from "vuex";

export default createStore({
  state: {
    selectedTask: null,
    tasks: []
  },
  getters: {
    allTasks: (state) => state.tasks
  },
  mutations: {
    updateState(state, data) {
      state.tasks = data
      localStorage.setItem('tasksList', JSON.stringify(data))
    },
    add_Task(state, task) {
      state.tasks.push(task);
      localStorage.setItem('tasksList', JSON.stringify(state.tasks))
    },
    delete_Task(state, id) {
      state.tasks = state.tasks.filter((task) => task.id != id);
      localStorage.setItem('tasksList', JSON.stringify(state.tasks))
    },
    update_Task(state, task) {
      let index = state.tasks.indexOf(state.tasks.find((x) => x?.id == task.id));
      index > -1 ? (state.tasks[index] = task) : null;
      localStorage.setItem('tasksList', JSON.stringify(state.tasks))
    }
  },
  actions: {
    addTask({ commit }, task) {
      commit("add_Task", task);
    },
    deleteTask({ commit }, task) {
      commit("delete_Task", task);
    },
    updateTask({ commit }, task) {
      commit("update_Task", task);
    }
  },
  modules: {},
});


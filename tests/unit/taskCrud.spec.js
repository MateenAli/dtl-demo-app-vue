import { mount } from '@vue/test-utils';
import taskCrud from '@/components/taskCrud.vue';


describe('taskCrud.vue', () => {
  it('will check every task should have toDoTask', async () => {
    const wrapper = mount(taskCrud);
    await wrapper.get('[data-test="todoTask"]')
    expect(wrapper.findAll('[data-test="todoTask"]')).toHaveLength(0)
  });
})
